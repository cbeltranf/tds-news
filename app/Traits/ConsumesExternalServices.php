<?php

namespace App\Traits;


trait ConsumesExternalServices
{

    /**
     * Send a request to a external service
     * @param string $method
     * @param string $requestUrl
     * @param array $formParams optional
     * @param array $header optional
     * @return string Respuesta del servicio
     */
    public function performRequest($method,  $requestUrl, $formParams=[], $headers=[])
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => $this->baseUri,
        ]);

        if(isset($this->secret)){
            $headers['Authorization'] = $this->secret;
        }
        //dd($headers);
        $response = $client->request($method, $requestUrl, ['form_params' => $formParams, 'headers' => $headers]);
        return $response->getBody()->getContents();
    }
}
