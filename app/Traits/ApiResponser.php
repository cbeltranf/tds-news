<?php

namespace App\Traits;
use Illuminate\Http\Response;

trait ApiResponser{

    /**
     * Return standard success response
     * @param  string|array $data information to send
     * @param int $code Http Response Code
    * @return \Illuminate\Http\JsonResponse
     */
    public function SuccessResponse($data, $code = Response::HTTP_OK){

        return response()->json(['data'=>$data], $code);
    }

    /**
     * Return standard error response
     * @param  string|array $message Text to error
     * @param int $code Http Response Code
    * @return \Illuminate\Http\JsonResponse
     */
    public function ErrorResponse($message, $code){

        return response()->json(['error'=>$message, 'code' => $code], $code);
    }
}
