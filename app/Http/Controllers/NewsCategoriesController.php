<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Models\NewsCategories;
use Illuminate\Http\Response;


class NewsCategoriesController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return NewsCategories List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news_categories  = NewsCategories::all();
        return $this->SuccessResponse($news_categories);
    }


    /**
     * Store NewsCategories item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'color' => ['required', 'regex:/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/'],
            'status' => 'required|in:A,S'
        ];

        $this->validate($request, $rules);
        $news_categories  = NewsCategories::create($request->all());
        return $this->SuccessResponse($news_categories, Response::HTTP_CREATED);
    }


    /**
     * Show NewsCategories item.
     *
     * @param  \App\Models\NewsCategories $news_categories
     * @return \Illuminate\Http\Response
     */
    public function show($news_categories)
    {
        $news_categories  = NewsCategories::findOrFail($news_categories);
        return $this->SuccessResponse($news_categories);
    }



    /**
     * Update NewsCategories item.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\NewsCategories $news_categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $news_categories)
    {
        $rules = [
            'name' => 'required',
            'color' => ['required', 'regex:/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/'],
            'status' => 'required|in:A,S'
        ];

        $this->validate($request, $rules);

        $news_categories  = NewsCategories::findOrFail($news_categories);
        $news_categories->fill($request->all());

        if ($news_categories->isClean()) {
            return $this->ErrorResponse('Al menos un valor debe cambiar', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $news_categories->save();
        return $this->ValidResponse($news_categories);
    }



    /**
     * SoftDelte NewsCategories item.
     *
     * @param  \App\Models\NewsCategories $news_categories
     * @return \Illuminate\Http\Response
     */
    public function destroy($news_categories)
    {
        $news_categories  = NewsCategories::findOrFail($news_categories);
        $news_categories->delete();
        return $this->SuccessResponse($news_categories);
    }

    //
}
