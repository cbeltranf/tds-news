<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Models\News;
use Illuminate\Http\Response;


class NewsController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return News List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news  = News::all();
        return $this->SuccessResponse($news);
    }


    /**
     * Store News item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user_id' => 'required',
            'news_category_id' => 'required|exists:news_categories,id',
            'news_gallery_id' => 'nullable|exists:news_galleries,id',
            'branch_id' => 'required',
            'title' => 'required',
            'text_short' => 'required',
            'text' => 'required',
            'photo' => 'required',
            'close_date' => 'nullable',
            'post_to' => 'required',
            'status' => 'required|in:A,S'
        ];

        $this->validate($request, $rules);
        $news  = News::create($request->all());
        return $this->SuccessResponse($news, Response::HTTP_CREATED);
    }


    /**
     * Show News item.
     *
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\Response
     */
    public function show($news)
    {
        $news  = News::findOrFail($news);
        return $this->SuccessResponse($news);
    }



    /**
     * Update News item.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $news)
    {
        $rules = [
            'user_id' => 'nullable',
            'news_category_id' => 'required|exists:news_categories,id',
            'news_gallery_id' => 'nullable|exists:news_galleries,id',
            'branch_id' => 'nullable',
            'title' => 'nullable',
            'text_short' => 'nullable',
            'text' => 'nullable',
            'photo' => 'nullable',
            'close_date' => 'nullable',
            'post_to' => 'nullable',
            'status' => 'nullable|in:A,S'
        ];
        $this->validate($request, $rules);

        $news  = News::findOrFail($news);
        $news->fill($request->all());

        if ($news->isClean()) {
            return $this->ErrorResponse('Al menos un valor debe cambiar', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $news->save();
        return $this->ValidResponse($news);
    }



    /**
     * SoftDelte News item.
     *
     * @param  \App\Models\News $news
     * @return \Illuminate\Http\Response
     */
    public function destroy($news)
    {
        $news  = News::findOrFail($news);
        $news->delete();
        return $this->SuccessResponse($news);
    }

    //
}
