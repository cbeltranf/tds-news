<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Models\NewsGalleries;
use Illuminate\Http\Response;


class NewsGalleriesController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return NewsGalleries List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news_galleries  = NewsGalleries::all();
        return $this->SuccessResponse($news_galleries);
    }


    /**
     * Store NewsGalleries item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user_id' => 'required',
            'name' => 'required',
            'description' => 'nullable',
            'status' => 'required|in:A,S'
        ];

        $this->validate($request, $rules);
        $news_galleries  = NewsGalleries::create($request->all());
        return $this->SuccessResponse($news_galleries, Response::HTTP_CREATED);
    }


    /**
     * Show NewsGalleries item.
     *
     * @param  \App\Models\NewsGalleries $news_galleries
     * @return \Illuminate\Http\Response
     */
    public function show($news_galleries)
    {
        $news_galleries  = NewsGalleries::findOrFail($news_galleries);
        return $this->SuccessResponse($news_galleries);
    }



    /**
     * Update NewsGalleries item.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\NewsGalleries $news_galleries
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $news_galleries)
    {
        $rules = [
            'user_id' => 'required',
            'name' => 'required',
            'description' => 'nullable',
            'status' => 'required|in:A,S'
        ];

        $this->validate($request, $rules);

        $news_galleries  = NewsGalleries::findOrFail($news_galleries);
        $news_galleries->fill($request->all());

        if ($news_galleries->isClean()) {
            return $this->ErrorResponse('Al menos un valor debe cambiar', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $news_galleries->save();
        return $this->ValidResponse($news_galleries);
    }



    /**
     * SoftDelte NewsGalleries item.
     *
     * @param  \App\Models\NewsGalleries $news_galleries
     * @return \Illuminate\Http\Response
     */
    public function destroy($news_galleries)
    {
        $news_galleries  = NewsGalleries::findOrFail($news_galleries);
        $news_galleries->delete();
        return $this->SuccessResponse($news_galleries);
    }

    //
}
