<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Models\NewsGalleryImages;
use Illuminate\Http\Response;


class NewsGalleryImagesController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return NewsGalleryImages List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news_gallery_images  = NewsGalleryImages::all();
        return $this->SuccessResponse($news_gallery_images);
    }


    /**
     * Store NewsGalleryImages item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user_id' => 'required',
            'news_category_id' => 'required|exists:news_categories,id',
            'news_gallery_id' => 'nullable|exists:news_galleries,id',
            'branch_id' => 'required',
            'title' => 'required',
            'text_short' => 'required',
            'text' => 'required',
            'photo' => 'required',
            'close_date' => 'nullable',
            'post_to' => 'required',
            'status' => 'required|in:A,S'
        ];

        $this->validate($request, $rules);
        $news_gallery_images  = NewsGalleryImages::create($request->all());
        return $this->SuccessResponse($news_gallery_images, Response::HTTP_CREATED);
    }


    /**
     * Show NewsGalleryImages item.
     *
     * @param  \App\Models\NewsGalleryImages $news_gallery_images
     * @return \Illuminate\Http\Response
     */
    public function show($news_gallery_images)
    {
        $news_gallery_images  = NewsGalleryImages::findOrFail($news_gallery_images);
        return $this->SuccessResponse($news_gallery_images);
    }



    /**
     * Update NewsGalleryImages item.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\NewsGalleryImages $news_gallery_images
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $news_gallery_images)
    {
        $rules = [
            'user_id' => 'nullable',
            'news_category_id' => 'required|exists:news_categories,id',
            'news_gallery_id' => 'nullable|exists:news_galleries,id',
            'branch_id' => 'nullable',
            'title' => 'nullable',
            'text_short' => 'nullable',
            'text' => 'nullable',
            'photo' => 'nullable',
            'close_date' => 'nullable',
            'post_to' => 'nullable',
            'status' => 'nullable|in:A,S'
        ];
        $this->validate($request, $rules);

        $news_gallery_images  = NewsGalleryImages::findOrFail($news_gallery_images);
        $news_gallery_images->fill($request->all());

        if ($news_gallery_images->isClean()) {
            return $this->ErrorResponse('Al menos un valor debe cambiar', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $news_gallery_images->save();
        return $this->ValidResponse($news_gallery_images);
    }



    /**
     * SoftDelte NewsGalleryImages item.
     *
     * @param  \App\Models\NewsGalleryImages $news_gallery_images
     * @return \Illuminate\Http\Response
     */
    public function destroy($news_gallery_images)
    {
        $news_gallery_images  = NewsGalleryImages::findOrFail($news_gallery_images);
        $news_gallery_images->delete();
        return $this->SuccessResponse($news_gallery_images);
    }

    //
}
