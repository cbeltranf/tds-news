<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "news";
    protected $fillable = [
        'user_id', 'news_category_id', 'news_gallery_id', 'branch_id', 'title', 'text_short', 'text', 'photo', 'publication_date', 'close_date', 'post_to', 'status'
    ];

}
