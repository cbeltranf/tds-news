<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsGalleryImages extends Model{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "news_gallery_images";
    protected $fillable = [
        'news_gallery_id', 'image'
    ];

}
