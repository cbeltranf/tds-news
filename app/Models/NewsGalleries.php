<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NewsGalleries extends Model{

    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "news_galleries";
    protected $fillable = [
        'user_id', 'name', 'description', 'status'
    ];

}
