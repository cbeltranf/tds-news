<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version().' News';
});

$router->get('/news', 'NewsController@index');
$router->post('/news', 'NewsController@store');
$router->get('/news/{new}', 'NewsController@show');
$router->put('/news/{new}', 'NewsController@update');
$router->patch('/news/{new}', 'NewsController@update');
$router->delete('/news/{new}', 'NewsController@destroy');



$router->get('/news_categories', 'NewsCategoriesController@index');
$router->post('/news_categories', 'NewsCategoriesController@store');
$router->get('/news_categories/{news_categories}', 'NewsCategoriesController@show');
$router->put('/news_categories/{news_categories}', 'NewsCategoriesController@update');
$router->patch('/news_categories/{news_categories}', 'NewsCategoriesController@update');
$router->delete('/news_categories/{news_categories}', 'NewsCategoriesController@destroy');


$router->get('/news_galleries', 'NewsGalleriesController@index');
$router->post('/news_galleries', 'NewsGalleriesController@store');
$router->get('/news_galleries/{news_galleries}', 'NewsGalleriesController@show');
$router->put('/news_galleries/{news_galleries}', 'NewsGalleriesController@update');
$router->patch('/news_galleries/{news_galleries}', 'NewsGalleriesController@update');
$router->delete('/news_galleries/{news_galleries}', 'NewsGalleriesController@destroy');


$router->get('/news_gallery_images', 'NewsGalleryImagesController@index');
$router->post('/news_gallery_images', 'NewsGalleryImagesController@store');
$router->get('/news_gallery_images/{news_gallery_images}', 'NewsGalleryImagesController@show');
$router->put('/news_gallery_images/{news_gallery_images}', 'NewsGalleryImagesController@update');
$router->patch('/news_gallery_images/{news_gallery_images}', 'NewsGalleryImagesController@update');
$router->delete('/news_gallery_images/{news_gallery_images}', 'NewsGalleryImagesController@destroy');

