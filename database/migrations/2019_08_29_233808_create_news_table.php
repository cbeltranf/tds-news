<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('news_category_id')->unsigned();
            $table->bigInteger('news_gallery_id')->unsigned()->nullable();
            $table->bigInteger('branch_id')->unsigned()->nullable();
            $table->string('title');
            $table->longText('text_short');
            $table->longText('text');
            $table->string('photo');
            $table->timestamp('publication_date')->useCurrent = true;
            $table->timestamp('close_date')->nullable();
            $table->string('post_to',255)->default('{"groups_to_publish":[],"branches_to_publish":[],"position_to_publish":[]}');
            $table->enum('status', ['A', 'C'])->default('A');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
